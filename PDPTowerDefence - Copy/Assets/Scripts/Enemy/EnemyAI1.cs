﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI1 : MonoBehaviour {
	
	private NavMeshAgent enemyAgent; //Enemy behaviour handler
	private GameObject target;

	// Use this for initialization
	void Start () {
		//Set up the target for enemy
		target = GameObject.Find("Checkpoint");
		enemyAgent = GetComponent<NavMeshAgent> ();
		enemyAgent.SetDestination (target.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (enemyAgent.remainingDistance <= 1) 
		{
			Destroy(gameObject);
		}
	}
}

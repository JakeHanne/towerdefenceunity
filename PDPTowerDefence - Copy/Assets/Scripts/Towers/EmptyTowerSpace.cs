﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyTowerSpace : MonoBehaviour {

	[SerializeField] private GameObject Tower;
	private Vector3 spawnPosition;
	private bool canBuild;

	void start()
	{
		canBuild = true;
	}
	void OnMouseOver()
	{
		GetComponentInChildren<ParticleSystem> ().Play ();
		if (Input.GetMouseButtonDown(0))
			{
			spawnPosition = new Vector3(transform.position.x, (transform.position.y + (Tower.transform.lossyScale.y / 2.0f)), transform.position.z);
			Instantiate (Tower, spawnPosition, Quaternion.identity);
			canBuild = false;
			}
	}

	void OnMouseExit()
	{
		GetComponentInChildren<ParticleSystem> ().Stop ();
	}

}

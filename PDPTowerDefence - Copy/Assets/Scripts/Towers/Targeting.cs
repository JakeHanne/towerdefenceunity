﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour {

	private GameObject targetEnemy;

	[SerializeField] private float rotSpeed;

	private Quaternion lookRotation;
	private Vector3 direction;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (targetEnemy != null) {

			Vector3 targetPos = new Vector3 (targetEnemy.transform.position.x, transform.position.y, targetEnemy.transform.position.z);
			direction = (targetPos - transform.position).normalized;
			lookRotation = Quaternion.LookRotation (direction);

			transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, Time.deltaTime * rotSpeed);
		}
			
	}

	void OnTriggerEnter(Collider col)
	{
		if ((col.gameObject.tag == "Enemy") && targetEnemy == null) 
		{
			targetEnemy = col.gameObject;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if ((col.gameObject.tag == "Enemy") && targetEnemy == col.gameObject) 
		{
			targetEnemy = null;
		}
	}
}

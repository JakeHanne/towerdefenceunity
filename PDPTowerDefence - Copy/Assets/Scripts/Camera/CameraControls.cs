﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {

	//Camera movement controls
	public float speedDir = 10.0f;
	Vector3 startPosDir;
	private Vector3 velocityDir = Vector3.zero;

	//ZoomIn
	float minZoomIn = 15f;
	float maxZoomOut = 60f;
	float zoomSensitivity = 10f;
	float startPosZoom;


	public float maxLeft;
	//public float max
	public float maxRight;
	public float maxUp;
	public float maxDown;

	//bool isReturning = false;
	// Use this for initialization
	void Start () {
		startPosDir = transform.position;
		startPosZoom = Camera.main.fieldOfView;
	}

	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey (KeyCode.D)) 
		{
			if ((transform.position.z < maxRight) && (transform.position.x < maxDown))
			transform.position += new Vector3 (speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.A)) 
		{
			if ((transform.position.z > maxLeft) && (transform.position.x > maxUp))
				transform.position -= new Vector3 (speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);

		}

		if (Input.GetKey (KeyCode.W)) 
		{
			if ((transform.position.z < maxRight) && (transform.position.x > maxUp))
			transform.position += new Vector3 (-speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.S)) 
		{
			if ((transform.position.z > maxLeft) && (transform.position.x < maxDown))
			transform.position -= new Vector3 (-speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.Space)) 
		{
			//isReturning = true;
			//transform.position = startPos;
			transform.position = Vector3.SmoothDamp (transform.position, startPosDir, ref velocityDir, 0.3f);
			transform.position = Vector3.SmoothDamp (transform.position, startPosDir, ref velocityDir, 0.3f);

			Camera.main.fieldOfView = (float)Mathf.Lerp (Camera.main.fieldOfView, startPosZoom, 0.3f);

		}



		//Zooming in and out
		float fov = Camera.main.fieldOfView;
		fov -= Input.GetAxis ("Mouse ScrollWheel") * zoomSensitivity;
		fov = Mathf.Clamp (fov, minZoomIn, maxZoomOut);
		Camera.main.fieldOfView = fov;

	}
}

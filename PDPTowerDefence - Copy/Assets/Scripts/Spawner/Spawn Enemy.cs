﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

	[SerializeField] private GameObject[] enemiesObj;
	// Use this for initialization
	void Start () {
		//InvokeRepeating()
		InvokeRepeating("spawnEnemy", 2.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {


	}

	void spawnEnemy()
	{
		Instantiate (enemiesObj [0], transform.position, Quaternion.identity);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnE : MonoBehaviour {

	[SerializeField] private GameObject enemiesObj;
	// Use this for initialization
	void Start () {
		InvokeRepeating("spawnEnemy", 2.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void spawnEnemy()
	{
		Instantiate (enemiesObj, transform.position, Quaternion.identity);
	}
}

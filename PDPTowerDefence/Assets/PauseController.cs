﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

	[SerializeField] private GameObject pauseUI;

	// Use this for initialization
	void Start () 
	{
		pauseUI.SetActive (false);
	}

	public void togglePauseScreen()
	{

		if (Time.timeScale == 1) 
		{
			Time.timeScale = 0;
		} 
		else 
		{
			Time.timeScale = 1;
		}

		pauseUI.SetActive (!pauseUI.activeSelf);
	}
		

}

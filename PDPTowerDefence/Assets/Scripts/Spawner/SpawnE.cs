﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnE : MonoBehaviour {

	[SerializeField] private GameObject[] enemiesObj;
    private GameManager gm;
    private int enemiesSpawned;

    private int enemiesPerWave;


	// Use this for initialization
	void Start () {
        gm = GameObject.Find("Game Manager").gameObject.GetComponent<GameManager>();
        //StartCoroutine(gm.GetComponent<MessageHandler>().showMessage("Wave Incoming!", 2.0f));
        enemiesSpawned = 0;
        enemiesPerWave = 5;
		InvokeRepeating("spawnEnemy", 2.0f, 2.0f);
    }
	
	// Update is called once per frame
	void Update () {
        if (enemiesSpawned >= enemiesPerWave)
        {
            CancelInvoke();
            enemiesSpawned = 0;
            enemiesPerWave += 2;
            StartCoroutine(nextWave());
        }
	}

    IEnumerator nextWave()
    {
        yield return new WaitForSeconds(10.0f);
        InvokeRepeating("spawnEnemy", 2.0f, 2.0f);
        StartCoroutine(gm.GetComponent<MessageHandler>().showMessage("Wave Incoming!", 2.0f));
    }

    void spawnEnemy()
	{
		int randomSelection = Random.Range (0, enemiesObj.Length + 5);
		switch (randomSelection) 
		{
		case 1:
			Instantiate (enemiesObj[0], transform.position, Quaternion.Euler(0, 180, 0));
                enemiesSpawned++;
			break;
		case 2:
			Instantiate (enemiesObj [0], transform.position, Quaternion.Euler (0, 180, 0));
                enemiesSpawned++;
                break;
		case 3:
			Instantiate (enemiesObj [0], transform.position, Quaternion.Euler (0, 180, 0));

                enemiesSpawned++;
                break;
		case 4:
			Instantiate (enemiesObj [1], transform.position, Quaternion.Euler (0, 180, 0));
                enemiesSpawned++;
                break;
		case 5:
			Instantiate (enemiesObj [1], transform.position, Quaternion.Euler (0, 180, 0));
                enemiesSpawned++;
                break;
		case 6:
			Instantiate (enemiesObj [2], transform.position, Quaternion.Euler (0, 180, 0));
                enemiesSpawned++;
                break;
		default:
			break;
		}
		//Instantiate (enemiesObj[0], transform.position, Quaternion.Euler(0, 180, 0));
	}
}

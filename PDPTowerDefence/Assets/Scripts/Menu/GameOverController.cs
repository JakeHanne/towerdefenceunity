﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour {

    [SerializeField] private GameObject gameOverUI;

    [SerializeField] private GameObject totalGold;
    [SerializeField] private InputField if_;


    // Use this for initialization
    void Start () {
        gameOverUI.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (gameOverUI.activeSelf == true)
        {
            totalGold.GetComponent<Text>().text = ("Gold gained: " + GetComponent<GameManager>().getOverallScore());

            if_.Select();
        }

	}

    public void showGameOverScreen()
    {
        gameOverUI.SetActive(true);
    }
}

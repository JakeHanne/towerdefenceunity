﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class enterNameGameOver : MonoBehaviour {

    [SerializeField] private InputField if_;

    private string playerName;
    [SerializeField] private int maxHighscores = 10;
    GameManager gm;


    // Use this for initialization
    void Start () {
        gm = GameObject.Find("Game Manager").gameObject.GetComponent<GameManager>();
        if_ = GetComponent<InputField>();
	}
	
	// Update is called once per frame
	void Update () {
        playerName = if_.text;

        if ((Input.GetKeyDown("return")))
        {
            if (playerName.Length <= 8 && playerName.Length != 0)
            {
                updateHighscores();
                Time.timeScale = 1;
                SceneManager.LoadScene("HighscoresScene");
            }
            else if (playerName.Length > 8)
            {
                if_.text = "MAX 8 CHARS!";
            }
        }
	}

    void updateHighscores()
    {
        //For storing old values when sorting
        string tempName;
        int tempScore;

        int pScore = gm.getOverallScore();

        //For every score in the leaderboard (10 for now)
        for (int i = 0; i < maxHighscores; i++)
        {
            //If scoreboard is already set up
            if (PlayerPrefs.HasKey("ScoreNo" + i))
            {
                //If player has beaten score at i
                if (pScore > PlayerPrefs.GetInt("ScoreNo" + i))
                {

                    //Store old name/score from i temporarily
                    tempName = PlayerPrefs.GetString("NameNo" + i);
                    tempScore = PlayerPrefs.GetInt("ScoreNo" + i);

                    //Move players score into its place
                    PlayerPrefs.SetString("NameNo" + i, playerName);
                    PlayerPrefs.SetInt("ScoreNo" + i, pScore);

                    //Set name/scoore equal to old values, before looping again
                    playerName = tempName;
                    pScore = tempScore;
                }
            }
            else
            {
                //If scoreboard isnt set up, add player to top of highscores
                PlayerPrefs.SetString("NameNo" + i, playerName);
                PlayerPrefs.SetInt("ScoreNo" + i, pScore);
                //After first iteration, fill the highscores with blank values
                pScore = 0;
                playerName = "";
            }
        }
        //Save all changes
        PlayerPrefs.Save();
    }
}

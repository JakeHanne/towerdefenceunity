﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loadHighscores : MonoBehaviour {

    public Text score1;
    public Text score2;
    public Text score3;
    public Text score4;
    public Text score5;
    public Text score6;
    public Text score7;
    public Text score8;
    public Text score9;
    public Text score10;

    // Use this for initialization
    void Start () {
        //If score is available in playerprefs
        showHighscores();
    }

    public void resetHighscores()
    {
        PlayerPrefs.DeleteAll();
        showHighscores();
    }

    public void showHighscores()
    {
        if (PlayerPrefs.HasKey("ScoreNo0"))
        {
            //Read in names/score and display
            score1.text = (PlayerPrefs.GetString("NameNo0") + " | " + PlayerPrefs.GetInt("ScoreNo0")).ToString();
            score2.text = (PlayerPrefs.GetString("NameNo1") + " | " + PlayerPrefs.GetInt("ScoreNo1")).ToString();
            score3.text = (PlayerPrefs.GetString("NameNo2") + " | " + PlayerPrefs.GetInt("ScoreNo2")).ToString();
            score4.text = (PlayerPrefs.GetString("NameNo3") + " | " + PlayerPrefs.GetInt("ScoreNo3")).ToString();
            score5.text = (PlayerPrefs.GetString("NameNo4") + " | " + PlayerPrefs.GetInt("ScoreNo4")).ToString();
            score6.text = (PlayerPrefs.GetString("NameNo5") + " | " + PlayerPrefs.GetInt("ScoreNo5")).ToString();
            score7.text = (PlayerPrefs.GetString("NameNo6") + " | " + PlayerPrefs.GetInt("ScoreNo6")).ToString();
            score8.text = (PlayerPrefs.GetString("NameNo7") + " | " + PlayerPrefs.GetInt("ScoreNo7")).ToString();
            score9.text = (PlayerPrefs.GetString("NameNo8") + " | " + PlayerPrefs.GetInt("ScoreNo8")).ToString();
            score10.text = (PlayerPrefs.GetString("NameNo9") + " | " + PlayerPrefs.GetInt("ScoreNo9")).ToString();

        }
        else
        { //If score cant be found
            for (int i = 0; i < 10; i++)
            {
                //Set all scores to 0 (Incase player accesses leaderboard before)
                PlayerPrefs.SetString("NameNo" + i, "Empty");
                PlayerPrefs.SetInt("ScoreNo" + i, 0);
            }
            //Save ammendments
            PlayerPrefs.Save();
            score1.text = (PlayerPrefs.GetString("NameNo0") + " | " + PlayerPrefs.GetInt("ScoreNo0")).ToString();
            score2.text = (PlayerPrefs.GetString("NameNo1") + " | " + PlayerPrefs.GetInt("ScoreNo1")).ToString();
            score3.text = (PlayerPrefs.GetString("NameNo2") + " | " + PlayerPrefs.GetInt("ScoreNo2")).ToString();
            score4.text = (PlayerPrefs.GetString("NameNo3") + " | " + PlayerPrefs.GetInt("ScoreNo3")).ToString();
            score5.text = (PlayerPrefs.GetString("NameNo4") + " | " + PlayerPrefs.GetInt("ScoreNo4")).ToString();
            score6.text = (PlayerPrefs.GetString("NameNo5") + " | " + PlayerPrefs.GetInt("ScoreNo5")).ToString();
            score7.text = (PlayerPrefs.GetString("NameNo6") + " | " + PlayerPrefs.GetInt("ScoreNo6")).ToString();
            score8.text = (PlayerPrefs.GetString("NameNo7") + " | " + PlayerPrefs.GetInt("ScoreNo7")).ToString();
            score9.text = (PlayerPrefs.GetString("NameNo8") + " | " + PlayerPrefs.GetInt("ScoreNo8")).ToString();
            score10.text = (PlayerPrefs.GetString("NameNo9") + " | " + PlayerPrefs.GetInt("ScoreNo9")).ToString();
        }
    }
}

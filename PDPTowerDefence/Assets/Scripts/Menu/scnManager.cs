﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scnManager : MonoBehaviour {

	public void playGame()
	{
		Debug.Log ("Play game");
		Time.timeScale = 1;
		SceneManager.LoadScene ("GameScene");
	}


	public void quitGame()
	{
		Debug.Log ("Quit");
		Application.Quit ();
	}

	public void showRules ()
	{
		SceneManager.LoadScene ("RulesScene");
	}

	public void showScores ()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("HighscoresScene");
	}

	public void returnToMenu()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene ("MenuScene");
	}
}

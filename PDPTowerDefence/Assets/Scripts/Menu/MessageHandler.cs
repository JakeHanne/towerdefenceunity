﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageHandler : MonoBehaviour {

    [SerializeField] private Text messageBox;

    void Start()
    {
        messageBox.enabled = false;
    }

    public IEnumerator showMessage(string textToDisplay, float delay)
    {
        messageBox.text = textToDisplay;
        messageBox.enabled = true;
        Debug.Log("SHOW MESSAGE");
        yield return new WaitForSeconds(delay);
        Debug.Log("HIDE MESSAGE");
        messageBox.enabled = false;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBomb : MonoBehaviour {

	private Rigidbody rb;
	private Vector3 target;
	private int projectileDamage;

	[SerializeField] private GameObject deathEffect;

	// Use this for initialization
	void Start () 
	{
		
	}

	void Update()
	{

	}

	public void setTarget(Vector3 t)
	{
		rb = GetComponent<Rigidbody> ();

		Vector3 targetDirection = t - transform.position;
		float heightDif = targetDirection.y;
		float distance = targetDirection.magnitude;
		float angle = 45 * Mathf.Deg2Rad;
		targetDirection.y = distance * Mathf.Tan (angle);
		distance += heightDif / Mathf.Tan (angle);

		target = (Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * angle))) * targetDirection.normalized;
		rb.velocity = target;
	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Terrain") 
		{
			destroyProjectile ();
		}
	}

	public void setProjectileDamage(int x)
	{
		projectileDamage = x;
	}

	public int getProjectileDamage()
	{
		return projectileDamage;
	}

	public void destroyProjectile()
	{
		gameObject.GetComponent<SphereCollider> ().enabled = true;
		GameObject effect = Instantiate (deathEffect, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
		Destroy(effect, 1.0f);
		Destroy(gameObject, .1f);
	}
}

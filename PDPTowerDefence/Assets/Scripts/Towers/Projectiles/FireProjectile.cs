﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireProjectile : MonoBehaviour {

	private Rigidbody rb;
	private GameObject target;
	private float projectileSpeed;
	private int projectileDamage;
	private Vector3 previousForce;

	[SerializeField] private GameObject deathEffect;

	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (target != null) {
			rb.velocity = new Vector3 (0.0f, 0.0f, 0.0f);
			rb.AddForce ((target.transform.position - transform.position).normalized * projectileSpeed);
			previousForce = rb.velocity;
		} else {
			rb.AddForce (previousForce);
			if (rb.velocity == new Vector3(0, 0, 0))
				destroyProjectile ();
		}

	}

	public void setTarget(GameObject t)
	{
		target = t;

	}

	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Terrain") 
		{
			destroyProjectile ();
		}
	}

	public void setProjectileSpeed(int x)
	{
		projectileSpeed = x;
	}

	public void setProjectileDamage(int x)
	{
		projectileDamage = x;
	}

	public int getProjectileDamage()
	{
		return projectileDamage;
	}

	public void destroyProjectile()
	{
		GameObject effect = Instantiate (deathEffect, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), Quaternion.identity);
		Destroy(effect, 1.0f);
		Destroy(gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyTowerSpace : MonoBehaviour
{

    [SerializeField] private GameObject Tower1;
    [SerializeField] private GameObject Tower2;
    [SerializeField] private GameObject Tower3;

    private GameManager gameManager;
    private CameraControls camera;

    //ShopVariables
    private ShopController shop;

    private Vector3 spawnPosition;

    private ParticleSystem[] particles;

    private int particleColourIndex;

    private bool isEmptySpace;  //empty space for a tower - green particles/red particles

    //Variables for the tower
    private int towerBuilt = 0;
    Transform cameraTarget;

    void Start()
    {
        gameManager = GameObject.Find("Game Manager").gameObject.GetComponent<GameManager>();
        camera = GameObject.Find("Main Camera").gameObject.GetComponent<CameraControls>();
        shop = GameObject.Find("Game Manager").gameObject.GetComponent<ShopController>();
        particleColourIndex = 0;
        particles = GetComponentsInChildren<ParticleSystem>();
        isEmptySpace = true;
    }
    void Update()
    {
        var mainFloating = particles[0].main;
        var mainRing = particles[1].main;

        switch (particleColourIndex)
        {
            case 0:
                mainRing.startColor = Color.white;
                mainFloating.startColor = Color.white;
                break;
            case 1:
                mainRing.startColor = Color.green;
                mainFloating.startColor = Color.green;
                break;
            case 2:
                mainRing.startColor = Color.red;
                mainFloating.startColor = Color.red;
                break;
            case 3:
                mainRing.startColor = Color.yellow;
                mainFloating.startColor = Color.yellow;
                break;
        }


    }


    void OnMouseOver()
    {
        particles[0].Play();
        if (isEmptySpace)
        {
            switch (gameManager.getTowerSelected()) //Get current tower selected
            {
                case 1: //Tower 1
                    if (gameManager.getPlayerScore() >= Tower1.GetComponent<Targeting>().getInitialPrice())
                    { //If can afford tower 1
                        particleColourIndex = 1;    //Make particles green
                    }
                    else particleColourIndex = 2;   //Make particles Red
                    break;
                case 2: //Tower 2
                    if (gameManager.getPlayerScore() >= Tower2.GetComponent<Targeting>().getInitialPrice()) //If can afford tower 2
                    {
                        particleColourIndex = 1;    //Make particles green
                    }
                    else particleColourIndex = 2;   //Make particles Red
                    break;
                case 3: //Tower 3
                    if (gameManager.getPlayerScore() >= Tower3.GetComponent<Targeting>().getInitialPrice()) //If can afford tower 3
                    {
                        particleColourIndex = 1;    //Make particles green
                    }
                    else particleColourIndex = 2;   //Make particles Red
                    break;
                default:
                    particleColourIndex = 0;    //Make particles white since none selected
                    break;

            }

            if (Input.GetMouseButtonDown(0) && (camera.getUpgradeMode() == false))
            {
                switch (gameManager.getTowerSelected())
                {
                    case 1:
                        if (gameManager.getPlayerScore() >= Tower1.GetComponent<Targeting>().getInitialPrice())
                        {
                            gameManager.changeScore(-Tower1.GetComponent<Targeting>().getInitialPrice());
                            spawnPosition = new Vector3(transform.position.x, (transform.position.y + (Tower1.transform.lossyScale.y / 2.0f) + 1.0f), transform.position.z -0.5f);
                            isEmptySpace = false;
                            towerBuilt = 1;
                            Instantiate(Tower1, spawnPosition, Quaternion.identity, transform);
                            particleColourIndex = 2;
                        }
                        break;
                    case 2:
                        if (gameManager.getPlayerScore() >= Tower2.GetComponent<Targeting>().getInitialPrice())
                        {
                            gameManager.changeScore(-Tower2.GetComponent<Targeting>().getInitialPrice());
                            spawnPosition = new Vector3(transform.position.x, (transform.position.y + (Tower2.transform.lossyScale.y / 2.0f)), transform.position.z);
                            isEmptySpace = false;
                            towerBuilt = 2;
                            Instantiate(Tower2, spawnPosition, Quaternion.identity, transform);
                            particleColourIndex = 2;
                        }
                        break;
                    case 3:
                        if (gameManager.getPlayerScore() >= Tower3.GetComponent<Targeting>().getInitialPrice())
                        {
                            gameManager.changeScore(-Tower3.GetComponent<Targeting>().getInitialPrice());
                            spawnPosition = new Vector3(transform.position.x, (transform.position.y + (Tower3.transform.lossyScale.y / 2.0f)), transform.position.z);
                            isEmptySpace = false;
                            towerBuilt = 3;
                            Instantiate(Tower3, spawnPosition, Quaternion.identity, transform);
                            particleColourIndex = 2;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            particleColourIndex = 3;

            //IF CAN UPGRADE
            if (Input.GetMouseButtonDown(0) && (camera.getUpgradeMode() == false))
            {
                switch (towerBuilt)
                {
                    case 1:
                        cameraTarget = transform.Find("Tower Basic(Clone)");
                        camera.showShop(cameraTarget);
                        shop.setupShop(towerBuilt, transform.Find("Tower Basic(Clone)").gameObject);
                        break;

                    case 2:
                        cameraTarget = transform.Find("Tower Slow(Clone)");
                        camera.showShop(cameraTarget);
                        shop.setupShop(towerBuilt, transform.Find("Tower Slow(Clone)").gameObject);
                        break;

                    case 3:
                        cameraTarget = transform.Find("Tower Launcher(Clone)");
                        camera.showShop(cameraTarget);
                        shop.setupShop(towerBuilt, transform.Find("Tower Launcher(Clone)").gameObject);
                        break;
                }
            }

        }
    }

    void OnMouseExit()
    {
        particleColourIndex = 0;
        particles[0].Stop();
    }

    public void resetTowerSpace()
    {
        towerBuilt = 0;
        isEmptySpace = true;
        particleColourIndex = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour
{

    private GameObject targetEnemy;

    [SerializeField] private float rotSpeed;
    [SerializeField] private float rateOfFire;
    [SerializeField] private GameObject Projectile;
    [SerializeField] private GameObject Emitter;
    [SerializeField] private int projectileSpeed;
    [SerializeField] private int projectileDamage;
    [SerializeField] private int initialPrice;
    [SerializeField] private float radius;
    [SerializeField] private int towerType;

    [SerializeField] private GameObject crystal;

    private int damageLevel;
    private int speedLevel;
    private int rotationLevel;

    private SphereCollider towerRange;
    private ParticleSystem ps;

    private Quaternion lookRotation;
    private Vector3 direction;
    private bool isShooting;

    // Use this for initialization
    void Start()
    {
        damageLevel = 1;
        speedLevel = 1;
        rotationLevel = 1;

        towerRange = GetComponent<SphereCollider>();
        towerRange.radius = radius;
        isShooting = false;
        if (towerType == 2)
        {
            ps = GetComponentInChildren<ParticleSystem>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay(Collider col)
    {
        switch (towerType)
        {
            case 1:
            case 3:
                if ((col.gameObject.tag == "Enemy") && targetEnemy == null)
                {
                    targetEnemy = col.gameObject;
                }
                else if (targetEnemy != null)
                {
                    Vector3 targetPos = new Vector3(targetEnemy.transform.position.x, crystal.transform.position.y, targetEnemy.transform.position.z);
                    direction = (targetPos - crystal.transform.position).normalized;
                    lookRotation = Quaternion.LookRotation(direction);

                   crystal.transform.rotation = Quaternion.Slerp(crystal.transform.rotation, lookRotation, Time.deltaTime * rotSpeed);

                    if (!isShooting)
                    {
                        InvokeRepeating("spawnProjectile", rateOfFire, rateOfFire);
                        isShooting = true;
                    }

                }
                else
                {
                    CancelInvoke();
                    isShooting = false;
                }

                break;
            default:
                break;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        switch (towerType)
        {
            case 2:
                if (col.gameObject.tag == "Enemy")
                {
                    targetEnemy = col.gameObject;
                    targetEnemy.GetComponent<EnemyAI1>().changeMovespeed(0.5f);
                    if (!ps.isPlaying)
                        ps.Play();
                }
                break;
            default:
                break;
        }
    }

    void OnTriggerExit(Collider col)
    {
        switch (towerType)
        {
            case 1:
            case 3:
                if ((col.gameObject.tag == "Enemy") && targetEnemy == col.gameObject)
                {
                    targetEnemy = null;
                }
                break;
            case 2:
                if (col.gameObject.tag == "Enemy")
                {
                    targetEnemy = col.gameObject;
                    targetEnemy.GetComponent<EnemyAI1>().changeMovespeed(2.0f);
                }
                break;
        }
    }

    void spawnProjectile()
    {
        GameObject tempProjectile = (GameObject)Instantiate(Projectile, Emitter.transform.position, Quaternion.identity);
        switch (towerType)
        {
            case 1:

                tempProjectile.GetComponent<FireProjectile>().setProjectileSpeed(projectileSpeed);
                tempProjectile.GetComponent<FireProjectile>().setProjectileDamage(projectileDamage);
                tempProjectile.GetComponent<FireProjectile>().setTarget(targetEnemy);
                break;
            case 3:
                tempProjectile.GetComponent<FireBomb>().setProjectileDamage(projectileDamage);
                tempProjectile.GetComponent<FireBomb>().setTarget(targetEnemy.transform.position);
                break;
        }
    }

    public int getInitialPrice()
    {
        return initialPrice;
    }

    public int getRateOfFire()
    {
        return speedLevel;
    }

    public int getDamage()
    {
        return damageLevel;
    }

    public int getRotation()
    {
        return rotationLevel;
    }

    public void levelRateOfFire()
    {
        speedLevel += 1;
        rateOfFire *= (rateOfFire / 0.9f);
    }

    public void levelDamage()
    {
        damageLevel += 1;
        projectileDamage += 2;
    }

    public void levelRotation()
    {
        rotationLevel += 1;
        rotSpeed += 2.0f;
    }
}

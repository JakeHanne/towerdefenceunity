﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBobbing : MonoBehaviour {

    Rigidbody rb_;
    Vector3 startPos;
	// Use this for initialization
	void Start () {
        rb_ = GetComponent<Rigidbody>();

        rb_.velocity = new Vector3(0.0f, 1.0f, 0.0f);

        startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.position.y - startPos.y > 1)
        {
            rb_.velocity = new Vector3(0.0f, -0.8f, 0.0f);
        }

        if (transform.position.y - startPos.y < -1)
        {
            rb_.velocity = new Vector3(0.0f, 0.8f, 0.0f);
        }
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{

    [SerializeField] private float maxHealth;
    [SerializeField] private GameObject healthBar;
    [SerializeField] private Text currencyText;


    //Tower selection
    private int towerSelected;

    [SerializeField] private GameObject tower1Img;
    [SerializeField] private GameObject tower2Img;
    [SerializeField] private GameObject tower3Img;

    [SerializeField] private GameObject tower1Bg;
    [SerializeField] private GameObject tower2Bg;
    [SerializeField] private GameObject tower3Bg;

    [SerializeField] private GameObject tower1;
    [SerializeField] private GameObject tower2;
    [SerializeField] private GameObject tower3;

    [SerializeField] private GameObject Enemy1;
    [SerializeField] private GameObject Enemy2;
    [SerializeField] private GameObject Enemy3;

    [SerializeField] private Text towerSelectedText;

    private float UIScaleSpeed = 10.0f;
    private float countUpSpeed = 6.0f;
    private float HealthScaleSpeed = 5.0f;
    private float ColChangeSpeed = 5.0f;

    private float currentHealth;
    private int currentScore;
    private int overallScore;
    private int progressionScore;

    private Vector3 enlargeScale = new Vector3(1.1f, 1.1f, 1.1f);
    private Vector3 originalScale = new Vector3(1.0f, 1.0f, 1.0f);

    void Start()
    {

        currentHealth = maxHealth;
        currentScore = 10;
        overallScore = 10;
        progressionScore = 10;
        currencyText.text = "Gold: " + currentScore.ToString();
        towerSelectedText.text = "Tower Selected: NONE";
    }

    public void decrementBaseHealth(float x)
    {

        if (currentHealth > 0.0f)
        {
            currentHealth = currentHealth - x;
            if (currentHealth < 0.0f)
                currentHealth = 0.0f;
            float unitHealth = currentHealth / maxHealth;
            setHealthBar(unitHealth);
        }

    }

    public void changeScore(int x)
    {
        if (x > 0)
        {
            overallScore += x;
            progressionScore += x;
        }

        if (progressionScore >= 50)
        {
            progressionScore = 0;
            Debug.Log("SHOW MESSAGE");
            StartCoroutine(GetComponent<MessageHandler>().showMessage("~ Enemies Strengthen ~", 3.0f));
            Enemy1.GetComponent<EnemyAI1>().addHealthFactor();
            Enemy2.GetComponent<EnemyAI1>().addHealthFactor();
            Enemy3.GetComponent<EnemyAI1>().addHealthFactor();
        }

          StartCoroutine(countUp(currentScore, currentScore + x));
    }

    public void setHealthBar(float health)
    {
        Vector3 temp = new Vector3(Mathf.Clamp(health, 0.0f, 1.0f), healthBar.transform.localScale.y, healthBar.transform.localScale.z);
        StartCoroutine(LerpHealth(healthBar, healthBar.transform.localScale, temp));
    }

    public int getTowerSelected()
    {
        return towerSelected;
    }

    public int getPlayerScore()
    {
        return currentScore;
    }

    public int getOverallScore()
    {
        return overallScore;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
			showTower1 ();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
			showTower2 ();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
			showTower3 ();
        }

        if ((currentScore >= tower1.GetComponent<Targeting>().getInitialPrice()) && (tower1Bg.GetComponent<Image>().color == Color.red))
        {
            StartCoroutine(LerpColour(tower1Bg, Color.red, Color.green));
        }
        else if ((currentScore < tower1.GetComponent<Targeting>().getInitialPrice()) && (tower1Bg.GetComponent<Image>().color == Color.green))
        {
            StartCoroutine(LerpColour(tower1Bg, Color.green, Color.red));
        }

        if ((currentScore >= tower2.GetComponent<Targeting>().getInitialPrice()) && (tower2Bg.GetComponent<Image>().color == Color.red))
        {
            StartCoroutine(LerpColour(tower2Bg, Color.red, Color.green));
        }
        else if ((currentScore < tower2.GetComponent<Targeting>().getInitialPrice()) && (tower2Bg.GetComponent<Image>().color == Color.green))
        {
            StartCoroutine(LerpColour(tower2Bg, Color.green, Color.red));
        }

        if ((currentScore >= tower3.GetComponent<Targeting>().getInitialPrice()) && (tower3Bg.GetComponent<Image>().color == Color.red))
        {
            StartCoroutine(LerpColour(tower3Bg, Color.red, Color.green));
        }
        else if ((currentScore < tower3.GetComponent<Targeting>().getInitialPrice()) && (tower3Bg.GetComponent<Image>().color == Color.green))
        {
            StartCoroutine(LerpColour(tower3Bg, Color.green, Color.red));
        }

		if (Input.GetKeyDown (KeyCode.P))
		{
			
			GetComponent<PauseController>().togglePauseScreen();
		
		}




    }

    IEnumerator countUp(int startScore, int endScore)
    {

        float progress = 0.0f;

        while (progress <= 1)
        {
            int tempScore = (int)Mathf.Lerp(startScore, endScore, progress);
            currencyText.text = "Gold: " + tempScore.ToString();
            progress += Time.deltaTime * countUpSpeed;
            yield return null;
        }
        currentScore = endScore;
        currencyText.text = "Gold: " + endScore.ToString();
    }

    IEnumerator LerpUp(GameObject obj, Vector3 initialScale, Vector3 finalScale)
    {
        float progress = 0.0f;

        while (progress <= 1)
        {
            obj.transform.localScale = Vector3.Lerp(initialScale, finalScale, progress);
            progress += Time.deltaTime * UIScaleSpeed;
            yield return null;
        }

        obj.transform.localScale = finalScale;
    }

    IEnumerator LerpHealth(GameObject obj, Vector3 initialScale, Vector3 finalScale)
    {
        float progress = 0.0f;

        while (progress <= 1)
        {
            obj.transform.localScale = Vector3.Lerp(initialScale, finalScale, progress);
            progress += Time.deltaTime * HealthScaleSpeed;
            yield return null;
        }

        obj.transform.localScale = finalScale;
        if (currentHealth == 0.0f)
        {
            Time.timeScale = 0;
            GetComponent<GameOverController>().showGameOverScreen();
        }
    }

    IEnumerator LerpColour(GameObject obj, Color startCol, Color endCol)
    {
        float progress = 0.0f;

        while (progress <= 1)
        {
            obj.GetComponent<Image>().color = Color.Lerp(startCol, endCol, progress);
            progress += Time.deltaTime * ColChangeSpeed;
            yield return null;
        }

        obj.GetComponent<Image>().color = endCol;
    }


	public void showTower1()
	{
		if (towerSelected == 1)
		{
			towerSelected = 0;
			StartCoroutine(LerpUp(tower1Img, enlargeScale, originalScale));
			towerSelectedText.text = "Tower Selected: NONE";
		}
		else
		{
			if (towerSelected == 2)
				StartCoroutine(LerpUp(tower2Img, enlargeScale, originalScale));
			if (towerSelected == 3)
				StartCoroutine(LerpUp(tower3Img, enlargeScale, originalScale));

			towerSelected = 1;
			StartCoroutine(LerpUp(tower1Img, originalScale, enlargeScale));
			towerSelectedText.text = "Tower Selected: Tracking";
		}
	}

	public void showTower2()
	{
		if (towerSelected == 2)
		{
			towerSelected = 0;
			StartCoroutine(LerpUp(tower2Img, enlargeScale, originalScale));
			towerSelectedText.text = "Tower Selected: NONE";
		}
		else
		{
			if (towerSelected == 1)
				StartCoroutine(LerpUp(tower1Img, enlargeScale, originalScale));
			if (towerSelected == 3)
				StartCoroutine(LerpUp(tower3Img, enlargeScale, originalScale));

			towerSelected = 2;
			StartCoroutine(LerpUp(tower2Img, originalScale, enlargeScale));
			towerSelectedText.text = "Tower Selected: Slow";
		}
	}

	public void showTower3()
	{
		if (towerSelected == 3)
		{
			towerSelected = 0;
			StartCoroutine(LerpUp(tower3Img, enlargeScale, originalScale));
			towerSelectedText.text = "Tower Selected: NONE";
		}
		else
		{
			if (towerSelected == 1)
				StartCoroutine(LerpUp(tower1Img, enlargeScale, originalScale));
			if (towerSelected == 2)
				StartCoroutine(LerpUp(tower2Img, enlargeScale, originalScale));

			towerSelected = 3;
			StartCoroutine(LerpUp(tower3Img, originalScale, enlargeScale));
			towerSelectedText.text = "Tower Selected: Launcher";
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{

    [SerializeField] private GameObject ShopUI;
    [SerializeField] private Text towerTitle;

    [SerializeField] private Text towerDmgLv;
    [SerializeField] private Text towerSpdLv;
    [SerializeField] private Text towerRotLv;

    [SerializeField] private Text towerDmgPrice;
    [SerializeField] private Text towerSpdPrice;
    [SerializeField] private Text towerRotPrice;

    [SerializeField] private Text towerSellPrice;

    [SerializeField] private Button damageUpButton;
    [SerializeField] private Button speedUpButton;
    [SerializeField] private Button rotationUpButton;

    [SerializeField] private Button sellTowerButton;

    //Tower prefabs to get initial prices from (just useful for less manual changing)
    [SerializeField] private GameObject t1;
    [SerializeField] private GameObject t2;
    [SerializeField] private GameObject t3;
    [SerializeField] private Text t1initPrice;
    [SerializeField] private Text t2initPrice;
    [SerializeField] private Text t3initPrice;

    private GameObject towerUG;
    private GameManager gm;
    private CameraControls camera;

    private bool showShop;
    // Use this for initialization
    void Start()
    {
        Button btn1 = damageUpButton.GetComponent<Button>();
        btn1.onClick.AddListener(levelDamage);

        Button btn2 = speedUpButton.GetComponent<Button>();
        btn2.onClick.AddListener(levelSpeed);

        Button btn3 = rotationUpButton.GetComponent<Button>();
        btn3.onClick.AddListener(levelRotation);

        Button btn4 = sellTowerButton.GetComponent<Button>();
        btn4.onClick.AddListener(sellTower);

        t1initPrice.text = t1.GetComponent<Targeting>().getInitialPrice().ToString() + "g";
        t2initPrice.text = t2.GetComponent<Targeting>().getInitialPrice().ToString() + "g";
        t3initPrice.text = t3.GetComponent<Targeting>().getInitialPrice().ToString() + "g";

        gm = GameObject.Find("Game Manager").gameObject.GetComponent<GameManager>();
        camera = GameObject.Find("Main Camera").gameObject.GetComponent<CameraControls>();

        ShopUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        if (showShop)
        {
            //Make shop interface visible
            ShopUI.SetActive(true);

            //Highlight buttons based on if you can afford upgrades

            updateUIColours();

            //Condition to exit shop
            if (Input.GetKeyDown(KeyCode.Escape))
            {
				hideShop ();
            }
        }
    }

    void updateUIColours()
    {
        //If damage can be upgraded
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getDamage() * 20)
        {
            damageUpButton.image.color = new Color(0, 0.8f, 0, 1); //Colour green
        }
        else
        {
            damageUpButton.image.color = new Color(0.8f, 0, 0, 1);  //Colour red
        }

        //If damage can be upgraded
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getRateOfFire() * 20)
        {
            speedUpButton.image.color = new Color(0, 0.8f, 0, 1); //Colour green
        }
        else
        {
            speedUpButton.image.color = new Color(0.8f, 0, 0, 1);  //Colour red
        }

        //If damage can be upgraded
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getRotation() * 20)
        {
            rotationUpButton.image.color = new Color(0, 0.8f, 0, 1); //Colour green
        }
        else
        {
            rotationUpButton.image.color = new Color(0.8f, 0, 0, 1);  //Colour red
        }
    }

    public void setVisible(bool bl)
    {
        showShop = bl;
    }

    public void setupShop(int towerBuilt, GameObject tower)
    {
        towerUG = tower;
        switch (towerBuilt)
        {
            case 1:
                towerTitle.text = ("Upgrade: Tracking Tower");
                break;
            case 2:
                towerTitle.text = ("Upgrade: Slow Tower");
                break;
            case 3:
                towerTitle.text = ("Upgrade: Launcher Tower");
                break;
        }

        towerDmgLv.text = "Lv" + tower.GetComponent<Targeting>().getDamage();
        towerSpdLv.text = "Lv" + tower.GetComponent<Targeting>().getRateOfFire();
        towerRotLv.text = "Lv" + tower.GetComponent<Targeting>().getRotation();

        towerDmgPrice.text = (tower.GetComponent<Targeting>().getDamage() * 20) + "g";
        towerSpdPrice.text = (tower.GetComponent<Targeting>().getRateOfFire() * 20) + "g";
        towerRotPrice.text = (tower.GetComponent<Targeting>().getRotation() * 20) + "g";

        towerSellPrice.text = "Sell Tower (" + (int)(((((towerUG.GetComponent<Targeting>().getDamage() + towerUG.GetComponent<Targeting>().getRateOfFire() + towerUG.GetComponent<Targeting>().getRotation()) - 3) * 20) + towerUG.GetComponent<Targeting>().getInitialPrice()) * 0.7f) + " gold)";
    }

    void levelDamage()
    {
        //If player can afford upgrade
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getDamage() * 20)
        {
            gm.changeScore(-towerUG.GetComponent<Targeting>().getDamage() * 20);
            towerUG.GetComponent<Targeting>().levelDamage();
            towerDmgLv.text = "Lv" + towerUG.GetComponent<Targeting>().getDamage();
            towerDmgPrice.text = (towerUG.GetComponent<Targeting>().getDamage() * 20) + "g";

            towerSellPrice.text = "Sell Tower (" + (int)(((((towerUG.GetComponent<Targeting>().getDamage() + towerUG.GetComponent<Targeting>().getRateOfFire() + towerUG.GetComponent<Targeting>().getRotation()) - 3) * 20) + towerUG.GetComponent<Targeting>().getInitialPrice()) * 0.7f) + " gold)";
        }
    }

    void levelSpeed()
    {
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getRateOfFire() * 20)
        {

            gm.changeScore(-towerUG.GetComponent<Targeting>().getRateOfFire() * 20);
            towerUG.GetComponent<Targeting>().levelRateOfFire();
            towerSpdLv.text = "Lv" + towerUG.GetComponent<Targeting>().getRateOfFire();
            towerSpdPrice.text = (towerUG.GetComponent<Targeting>().getRateOfFire() * 20) + "g";

            towerSellPrice.text = "Sell Tower (" + (int)(((((towerUG.GetComponent<Targeting>().getDamage() + towerUG.GetComponent<Targeting>().getRateOfFire() + towerUG.GetComponent<Targeting>().getRotation()) - 3) * 20) + towerUG.GetComponent<Targeting>().getInitialPrice()) * 0.7f) + " gold)";
        }
    }

    void levelRotation()
    {
        if (gm.getPlayerScore() > towerUG.GetComponent<Targeting>().getRotation() * 20)
        {

            gm.changeScore(-towerUG.GetComponent<Targeting>().getRotation() * 20);
            towerUG.GetComponent<Targeting>().levelRotation();
            towerRotLv.text = "Lv" + towerUG.GetComponent<Targeting>().getRotation();
            towerRotPrice.text = (towerUG.GetComponent<Targeting>().getRotation() * 20) + "g";

            towerSellPrice.text = "Sell Tower (" + (int)(((((towerUG.GetComponent<Targeting>().getDamage() + towerUG.GetComponent<Targeting>().getRateOfFire() + towerUG.GetComponent<Targeting>().getRotation()) - 3) * 20) + towerUG.GetComponent<Targeting>().getInitialPrice()) * 0.7f) + " gold)";

        }
    }

    void sellTower()
    {
        //Give player money for their tower
        gm.changeScore((int)(((((towerUG.GetComponent<Targeting>().getDamage() + towerUG.GetComponent<Targeting>().getRateOfFire() + towerUG.GetComponent<Targeting>().getRotation()) - 3) * 20) + towerUG.GetComponent<Targeting>().getInitialPrice()) * 0.7f));
        towerUG.GetComponentInParent<EmptyTowerSpace>().resetTowerSpace();
        Destroy(towerUG);
        ShopUI.SetActive(false);
        showShop = false;
        camera.setUgradeMode(false);

    }

	public void hideShop()
	{
		
			//Make stuff invisible
			ShopUI.SetActive (false);
			showShop = false;

	}
}

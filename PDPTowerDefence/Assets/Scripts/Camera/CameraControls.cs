﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{

    //Camera movement controls
    public float speedDir = 10.0f;
    Vector3 startPosDir;
    private Vector3 velocityDir = Vector3.zero;

    //ZoomIn
    float minZoomIn = 15f;
    float maxZoomOut = 60f;
    float zoomSensitivity = 10f;
    float startPosZoom;


    public float maxLeft;
    //public float max
    public float maxRight;
    public float maxUp;
    public float maxDown;

    private bool restrictCameraMovement = false;
    private bool inUpgradeMode = false;
    private Vector3 targetPos;
    private Vector3 positionBeforeShop;
    private bool isZoomingOut;

    private ShopController shopControl;

    // Use this for initialization
    void Start()
    {
        shopControl = GameObject.Find("Game Manager").GetComponent<ShopController>();
        startPosDir = transform.position;
        startPosZoom = Camera.main.fieldOfView;
    }


    // Update is called once per frame
    void Update()
    {

        //IF player can move the camera
        if (!restrictCameraMovement)
        {

            if (!inUpgradeMode)
            {
                if (Input.GetKey(KeyCode.D))
                {
                     if ((transform.position.z < maxRight) && (transform.position.x < maxDown))
                        transform.position += new Vector3(speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.A))
                {
                    if ((transform.position.z > maxLeft) && (transform.position.x > maxUp))
                        transform.position -= new Vector3(speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);

                }

                if (Input.GetKey(KeyCode.W))
                {
                    if ((transform.position.z < maxRight) && (transform.position.x > maxUp))
                        transform.position += new Vector3(-speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.S))
                {
                    if ((transform.position.z > maxLeft) && (transform.position.x < maxDown))
                        transform.position -= new Vector3(-speedDir * Time.deltaTime, 0, speedDir * Time.deltaTime);
                }

                else if (Input.GetKey(KeyCode.Space))
                {
                    //isReturning = true;
                    //transform.position = startPos;
                    transform.position = Vector3.SmoothDamp(transform.position, startPosDir, ref velocityDir, 0.3f);
                    transform.position = Vector3.SmoothDamp(transform.position, startPosDir, ref velocityDir, 0.3f);

                    Camera.main.fieldOfView = (float)Mathf.Lerp(Camera.main.fieldOfView, startPosZoom, 0.3f);

                }

                //Zooming in and out
                float fov = Camera.main.fieldOfView;
                fov -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
                fov = Mathf.Clamp(fov, minZoomIn, maxZoomOut);
                Camera.main.fieldOfView = fov;
            }
        }
        else if (inUpgradeMode) //If camera movement is restricted && in upgrade mode
        {
            if (Input.GetKey(KeyCode.Escape)) //IF CONDITION FOR EXITING SHOP
            {
				returnCameraPosition ();
            }
        }

    }

	public void returnCameraPosition()
	{
		StartCoroutine(LerpCamera(positionBeforeShop, false, false));
	}

    public void showShop(Transform towerPos)
    {
        Vector3 tempPos2 = transform.position;
        positionBeforeShop = tempPos2;

        Vector3 tempPos = new Vector3(towerPos.position.x + 8.0f, towerPos.position.y + 8.0f, towerPos.position.z - 8.0f);
        targetPos = tempPos;

        StartCoroutine(LerpCamera(targetPos, true, true));
    }

    //MOVES CAMERA TOWARDS TARGET
    IEnumerator LerpCamera(Vector3 target, bool inUpgrade, bool restrictMovement)
    {
        inUpgradeMode = inUpgrade;
        restrictCameraMovement = restrictMovement;

        if (inUpgrade)
        {
            shopControl.setVisible(true);
        }

        float progress = 0.0f;

        while (progress <= 1.0f)
        {
            transform.position = Vector3.Lerp(transform.position, target, progress);
            progress += Time.deltaTime * 1.2f;
            yield return null;
        }


    }

    public void setUgradeMode(bool bl)
    {
        inUpgradeMode = bl;
        StartCoroutine(LerpCamera(positionBeforeShop, false, false));
    }

    public bool getUpgradeMode()
    {
        return inUpgradeMode;
    }
}

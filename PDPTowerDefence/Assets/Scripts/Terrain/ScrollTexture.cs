﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {

	public float scrollX = 0.5f;
	public float scrollZ = 0.5f;

	void Update () 
	{
		float offsetX = Time.time * scrollX;
		float offsetZ = Time.time * scrollZ;

		GetComponent<Renderer> ().material.SetTextureOffset("_MainTex", new Vector2 (offsetX, offsetZ));
		GetComponent<Renderer> ().material.SetTextureOffset("_BumpMap", new Vector2( offsetX, offsetZ));
		GetComponent<Renderer> ().material.SetTextureOffset("_Illum", new Vector2( offsetX, offsetZ));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI1 : MonoBehaviour {
	
	private NavMeshAgent enemyAgent; //Enemy behaviour handler
	private GameObject target;
	[SerializeField] private float damage;
	[SerializeField] private int goldValue;
	[SerializeField] private int maxHealth;
	[SerializeField] private float moveSpeed;

    private int healthScaleFactor =  0;
	private int currentHealth;

	// Use this for initialization
	void Start () 
	{
		//Set up the target for enemy
		target = GameObject.Find("Checkpoint");
		enemyAgent = GetComponent<NavMeshAgent> ();
		enemyAgent.SetDestination (target.transform.position);
        currentHealth = maxHealth + healthScaleFactor;
		enemyAgent.speed = moveSpeed;
	}

    public void addHealthFactor()
    {
		healthScaleFactor += 2;
    }

	// Update is called once per frame
	void Update () {
		
		if (Vector3.Distance(transform.position, target.transform.position) <= 5) 
		{
			GameObject.Find ("Game Manager").gameObject.GetComponent<GameManager> ().decrementBaseHealth (damage);
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Bullet") 
		{
			takeDamage(col.gameObject.GetComponent<FireProjectile>().getProjectileDamage());
			col.gameObject.GetComponent<FireProjectile>().destroyProjectile ();
		}
	}

	void OnTriggerStay(Collider col)
	{
		if (col.gameObject.tag == "Bomb") 
		{
			takeDamage(col.gameObject.GetComponent<FireBomb>().getProjectileDamage());
			col.gameObject.GetComponent<FireBomb>().destroyProjectile ();
		}
	}

	public void takeDamage(int x)
	{
		currentHealth -= x;
		if (currentHealth <= 0) 
		{
			GameObject.Find ("Game Manager").gameObject.GetComponent<GameManager> ().changeScore (goldValue);
			Destroy (gameObject);
		}
	}

	public void changeMovespeed(float x)
	{
		enemyAgent.speed *= x;
	}
}
